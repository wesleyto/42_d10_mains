/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/21 18:05:11 by wto               #+#    #+#             */
/*   Updated: 2017/08/21 18:12:50 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_map.c"

int plus_five(int n)
{
	return (n + 5);
}

int arrcmp(int *arr1, int*arr2, int size)
{
	if (arr1 == NULL && arr2 == NULL)
	{
		return (0);
	}
	else if (arr1 == NULL || arr2 == NULL)
	{
		return (-1);
	}
	else
	{
		for (int i = 0; i < size; i++)
		{
			if (arr1[i] != arr2[i])
			{
				return (arr1[i] - arr2[i]);
			}
		}
	}
	return (0);
}

void	print_arr(int *arr, int size)
{
	if (size == 0)
	{
		printf("[%s]", "");
	}
	else if (arr != NULL)
	{
		if (size == 1)
		{
			printf("[%d]", arr[0]);
		}
		else
		{
			printf("[%d, ", arr[0]);
			for (int j = 1; j < size - 1; j++)
			{
				printf("%d, ", arr[j]);
			}
			printf("%d]", arr[size-1]);
		}
	}
	else
	{
		printf("%-10s", "NULL");
	}
}

int	main(void)
{
	int nums1[] = {-10, -7, -4, -1, 0, 1, 3, 6, 9};
	int exps1[] = {-5, -2, 1, 4, 5, 6, 8, 11, 14};
	int nums2[] = {};
	int exps2[] = {};
	int nums3[] = {1};
	int exps3[] = {6};
	int len[] = {9, 0, 1};
	int *nums[] = {nums1, nums2, nums3};
	int *exps[] = {exps1, exps2, exps3};
	for (int i = 0; i < (int)(sizeof(len) / sizeof(int)); i++)
	{
		int *mapped = ft_map(nums[i], len[i], &plus_five);
		int result = arrcmp(mapped, exps[i], len[i]);
		printf("%s || %s", result == 0 ? "Success" : "Failure", "Case: ");
		print_arr(nums[i], len[i]);
		printf(" || %s", "Got: ");
		print_arr(mapped, len[i]);
		printf(" || %s", "Exp: ");
		print_arr(exps[i], len[i]);
		printf("%s", "\n");
	}
}

















