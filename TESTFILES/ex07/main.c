/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 16:11:08 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 16:16:55 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_split_whitespaces.c"
#include "ft_sort_wordtab.c"
#include <stdio.h>

int	main(void)
{
	char *str1 = "Sort Me Please Aardvark Aaron Zebra";
	char *exp1 = "Aardvark Aaron Me Please Sort Zebra";
	char *str2 = "Z Y X W V U T S R Q P O N M";
	char *exp2 = "M N O P Q R S T U V W X Y Z";
	char *str3 = "Aa Aaron Aardvark Aarghh Aa Aah Aarp";
	char *exp3 = "Aa Aa Aah Aardvark Aarghh Aaron Aarp";
	char *str4 = "Aa";
	char *exp4 = "Aa";
	char *str5 = "";
	char *exp5 = NULL;
	int i;
	char **tab;
	char *strs[] = {str1, str2, str3, str4, str5};
	char *exps[] = {exp1, exp2, exp3, exp4, exp5};

	for (int j = 0; j < 5; j++)
	{
		tab = ft_split_whitespaces(strs[j]);
		printf("Case:\t%s\nGot:\t", strs[j]);
		ft_sort_wordtab(tab);
		i = 0;
		if (!tab[i])
		{
			printf("%s\nExp:\t", tab[i]);
		}
		else
		{
			while (tab[i])
			{
				if (!tab[i + 1])
				{
					printf("%s\nExp:\t", tab[i]);
				}
				else
				{
					printf("%s ", tab[i]);
				}
				i++;
			}
		}
		if (j != 4)
		{
			printf("%s\n\n", exps[j]);
		}
		else
		{
			printf("%s\n", exps[j]);
		}
	}
	return (0);
}
