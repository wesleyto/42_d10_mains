/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/21 18:22:34 by wto               #+#    #+#             */
/*   Updated: 2017/08/21 18:53:23 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_count_if.c"
#include <stdio.h>

int	str_len(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		i++;
	}
	return (i);
}

void print_each(char **strs, int size)
{
	int i;

	i = 0;
	while (i < size)
	{
		printf("\"%s\"", strs[i++]);
		if (i != size)
		{
			printf("%s", " ");
		}
	}
}

int main(void)
{
	char *str1 = "Hello";
	char *str2 = "1";
	char *str3 = "";

	char *strs1[] = {str1, str2, str3, 0};
	char *strs2[] = {str1, str3, 0};
	char *strs3[] = {str2, 0};
	char *strs4[] = {str1, str2, 0};
	char *strs5[] = {str1, 0};
	char *strs6[] = {str3, 0};
	char *strs7[] = {str2, str2, 0};
	char *strs8[] = {str2, str3, str2, 0};
	char **cases[] = {strs1, strs2, strs3, strs4, strs5, strs6, strs7, strs8};
	int exps[] = {1, 0, 1, 1, 0, 0, 2, 2};
	int len[] = {4, 3, 2, 3, 2, 2, 3, 4};

	for (int i = 0; i < (int)(sizeof(exps) / sizeof(int)); i++)
	{
		int result = ft_count_if(cases[i], &str_len);
		char *res = result == exps[i] ? "Success" : "Failure";
		printf("%s || Got: %d || Exp: %d || Case: ", res, result, exps[i]);
		print_each(cases[i], len[i]);
		printf("%s", "\n");
	}
}
