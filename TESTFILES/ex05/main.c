/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/21 19:16:00 by wto               #+#    #+#             */
/*   Updated: 2017/08/21 19:23:50 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_is_sort.c"

int gt(int a, int b)
{
	return (a - b);
}

void	print_arr(int *arr, int size)
{
	if (size == 0)
	{
		printf("[%s]", "");
	}
	else if (arr != NULL)
	{
		if (size == 1)
		{
			printf("[%d]", arr[0]);
		}
		else
		{
			printf("[%d, ", arr[0]);
			for (int j = 1; j < size - 1; j++)
			{
				printf("%d, ", arr[j]);
			}
			printf("%d]", arr[size-1]);
		}
	}
	else
	{
		printf("%-10s", "NULL");
	}
}

int main(void)
{
	int nums1[] = {1, 2, 3, 4, 5};
	int nums2[] = {1, 3, 2, 4, 5};
	int nums3[] = {5, 4, 3, 2, 1};
	int nums4[] = {1, 2, 3, 5, 4};
	int nums5[] = {1};
	int nums6[] = {};

	int *nums[] = {nums1, nums2, nums3, nums4, nums5, nums6};
	int lens[] = {5, 5, 5, 5, 1, 0};
	int exps[] = {1, 0, 0, 0, 1, 1};

	for (int i = 0; i < (int)(sizeof(lens) / sizeof(int)); i++)
	{
		int result = ft_is_sort(nums[i], lens[i], &gt);
		char *res = result == exps[i] ? "Success" : "Failure";
		printf("%s || Got: %-10s || Exp: %-10s || Case: ", res, result == 1 ? "Sorted" : "Not Sorted", exps[i] == 1 ? "Sorted" : "Not Sorted");
		print_arr(nums[i], lens[i]);
		printf("%s", "\n");
	}
}
