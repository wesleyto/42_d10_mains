/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_whitespaces.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/16 17:08:11 by wto               #+#    #+#             */
/*   Updated: 2017/08/16 19:06:13 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		is_whitespace(char c)
{
	return (c == '\n' || c == '\t' || c == ' ' ? 1 : 0);
}

int		get_word_length(char *str, int start)
{
	int i;

	i = start;
	while (str[i] != '\0' && is_whitespace(str[i]) == 0)
	{
		i++;
	}
	return (i - start);
}

int		first_non_whitespace_index(char *str, int start)
{
	int i;

	i = start;
	while (str[i] != '\0' && is_whitespace(str[i]) == 1)
	{
		i++;
	}
	return (i);
}

int		get_num_words(char *str)
{
	int i;
	int num_words;

	i = first_non_whitespace_index(str, 0);
	num_words = 0;
	while (str[i] != '\0')
	{
		while (is_whitespace(str[i]) == 0 && str[i] != '\0')
		{
			i++;
		}
		num_words++;
		i = first_non_whitespace_index(str, i);
	}
	return (num_words);
}

char	**ft_split_whitespaces(char *str)
{
	int		i;
	int		len;
	char	**strs;
	int		wc;
	int		target;

	wc = 0;
	target = get_num_words(str);
	i = first_non_whitespace_index(str, 0);
	strs = malloc(sizeof(char *) * (target + 1));
	strs[target] = 0;
	while (wc < target)
	{
		len = get_word_length(str, i);
		strs[wc] = malloc(sizeof(char) * (len + 1));
		strs[wc][len--] = '\0';
		while (len >= 0)
		{
			strs[wc][len] = str[i + len];
			len--;
		}
		i = first_non_whitespace_index(str, i + get_word_length(str, i));
		wc++;
	}
	return (strs);
}
