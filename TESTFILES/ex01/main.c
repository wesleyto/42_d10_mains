/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/21 17:32:21 by wto               #+#    #+#             */
/*   Updated: 2017/08/21 17:42:58 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_foreach.c"
#include "ft_putnbr.h"
#include "ft_putnbr.c"
#include <stdio.h>

int		main(void)
{
	int nums1[] = {-10, -7, -4, -1, 0, 1, 3, 6, 9};
	int nums2[] = {};
	int nums3[] = {1};
	int len[] = {9, 0, 1};
	int *nums[] = {nums1, nums2, nums3};
	for (int i = 0; i < (int)(sizeof(len) / sizeof(int)); i++)
	{
		ft_foreach(nums[i], len[i], &ft_putnbr);
		printf("%s", "\n");
	}
}
