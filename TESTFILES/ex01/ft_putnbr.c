/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 08:58:15 by wto               #+#    #+#             */
/*   Updated: 2017/08/14 21:07:29 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_putnbr.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	print_positive(int n)
{
	int pseudostack[10];
	int i;

	i = -1;
	if (n == 0)
	{
		ft_putchar('0');
	}
	while (n > 0)
	{
		i++;
		pseudostack[i] = n % 10;
		n /= 10;
	}
	while (i >= 0)
	{
		ft_putchar('0' + pseudostack[i]);
		i--;
	}
}

void	print_negative(int n)
{
	if (n == -2147483648)
	{
		ft_putchar('-');
		print_positive(214748364);
		ft_putchar('8');
	}
	else
	{
		ft_putchar('-');
		print_positive(-1 * n);
	}
}

void	ft_putnbr(int nb)
{
	if (nb >= 0)
	{
		print_positive(nb);
	}
	else
	{
		print_negative(nb);
	}
}
