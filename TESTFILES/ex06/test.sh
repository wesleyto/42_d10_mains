#!/bin/bash
make -f Makefile
echo -n "N  => " ; ./do-op
echo
echo -n "8  => " ; ./do-op 5 + 3
echo -n "2  => " ; ./do-op 5 - 3
echo -n "1  => " ; ./do-op 5 / 3
echo -n "15 => " ; ./do-op 5 \* 3
echo -n "2  => " ; ./do-op 5 % 3
echo -n "D0 => " ; ./do-op 5 / 0
echo -n "M0 => " ; ./do-op 5 % 0
echo -n "8  => " ; ./do-op 5something + 3else
echo -n "0  => " ; ./do-op something5 + else3
echo -n "5  => " ; ./do-op 5something + else3
echo -n "3  => " ; ./do-op something5 + 3else
echo -n "0  => " ; ./do-op something not valid
echo
make clean
make fclean